CC = gcc
HDRS_DIR = include/
CFLAGS = -I$(HDRS_DIR)
SRC_DIR = src/
TARGET_DIR = $(SRC_DIR)test/
TARGET = test
DEP =	$(SRC_DIR)bouble_sort.o 	\
		$(SRC_DIR)pyramid_sort.o	\
		$(TARGET_DIR)$(TARGET).o

$(SRC_DIR)%.o : $(SRC_DIR)%.c
	$(CC) -c $< -o $@ $(CFLAGS)
$(TARGET) : $(DEP)
	$(CC) $^ -o $@