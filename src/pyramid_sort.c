#define swap(a, b) {					\
	__auto_type tmp = a;				\
	a = b;								\
	b = tmp;							\
}

void down_heap(int *arr, int k, int n) {
	int new_elem = arr[k];

	while (k <= n / 2) {
		int child = k * 2;
		if (child < n && arr[child] < arr[child + 1]) {
			child++;
		}
		if (new_elem >= arr[child]) {
			break;
		}
		arr[k] = arr[child];
		k = child;
	}
	arr[k] = new_elem;
}

void pyramid_sort(int *num, int n) {
	for (int i = n / 2; i >= 0; i--) {
		down_heap(num, i, n - 1);
	}
	while (--n > 0) {
		swap(num[0], num[n]);
		down_heap(num, 0, n - 1);
	}
}
