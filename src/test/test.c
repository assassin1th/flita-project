#include "bouble_sort.h"
#include "pyramid_sort.h"
#include <stdio.h>
#include <stdlib.h>

int *value_cpy(int *arr, int n) {
	int *new_arr = calloc(n, sizeof(int));
	for (int i = 0; i < n; i++) {
		new_arr[i] = arr[i];
	}
	return new_arr;
}

int arr_cmp(int *arr1, int *arr2, int n) {
	for (int i = 0; i < n; i++) {
		if (arr1[i] != arr2[i]) {
			return arr1[i] - arr2[i];
		}
	}
	return 0;
}

static void print(int *arr, int n) {
	for (int i = 0; i < n; i++) {
		printf((i + 1 == n) ? "%d" : "%d ", arr[i]);
	}
	printf("\n");
}

int main(int argc, char *argv[]) {
	int test_value[] = {1, 4, 3, 10, 21, 4, 10, 13, 12, 11, 30};
	int expected_value[] = {1, 3, 4, 4, 10, 10, 11, 12, 13, 21, 30};
	int len = sizeof(test_value) / sizeof(int);
	int *p = value_cpy(test_value, len);
	bouble_sort(p, len);
	print(expected_value, len);
	print(p, len);
	free(p);
	p = value_cpy(test_value, len);
	pyramid_sort(p, len);
	print(expected_value, len);
	print(p, len);
	free(p);
	return 0;
}
